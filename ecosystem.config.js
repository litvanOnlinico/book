module.exports = {
  apps: [
    {
      name: 'API',
      script: 'dist/main.js',

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: 'one two',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '500M',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],

  deploy: {
    dev: {
      user: 'root',
      host: 'book.litvan.com',
      ref: 'origin/book',
      repo: 'git@bitbucket.org:litvanOnlinico/book.git',
      path: '/root/www/book',
      'post-deploy':
        'npm install && npm run build && pm2 reload ecosystem.config.js',
    },
  },
};
