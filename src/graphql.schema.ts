/* tslint:disable */
export abstract class CreateCatInput {
    name?: string;
    age?: number;
}

export abstract class InputCreateUser {
    name: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
}

export abstract class InputLoginUser {
    email: string;
    password: string;
}

export abstract class Cat {
    id?: number;
    name?: string;
    age?: number;
}

export abstract class Collaboration {
    id: string;
    name: string;
    members: CollaborationMember[];
    currentMember?: CollaborationMember;
}

export abstract class CollaborationMember {
    id: string;
    role?: string;
    user?: User;
    collaboration?: Collaboration;
}

export abstract class IMutation {
    abstract createCat(createCatInput?: CreateCatInput): Cat | Promise<Cat>;

    abstract createUser(input?: InputCreateUser): User | Promise<User>;
}

export abstract class IQuery {
    abstract getCats(): Cat[] | Promise<Cat[]>;

    abstract cat(id: string): Cat | Promise<Cat>;

    abstract collaborations(): Collaboration[] | Promise<Collaboration[]>;

    abstract getUsers(): User[] | Promise<User[]>;

    abstract me(): User | Promise<User>;

    abstract login(input?: InputLoginUser): string | Promise<string>;

    abstract resetPassword(email?: string): boolean | Promise<boolean>;

    abstract temp__(): boolean | Promise<boolean>;
}

export abstract class ISubscription {
    abstract catCreated(): Cat | Promise<Cat>;
}

export abstract class User {
    id: string;
    name: string;
    email: string;
    firstName: string;
    lastName: string;
    emailIsConfirmed?: boolean;
}
