import { Injectable, Logger } from '@nestjs/common';
import { Connection, EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';
import { UserRepository } from '../repositories/user.repository';
import { CollaborationRepository } from '../repositories/collaboration.repository';
import { CollaborationMemberRepository } from '../repositories/collaboration-member.repository';
import { User } from '../entities/user.entity';
import { Collaboration } from '../entities/collaboration.entity';
import { CollabRolesEnum, CollaborationMember } from '../entities/collaboration-member.entity';

@Injectable()
@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<User> {
  private readonly logger = new Logger(self.name);

  constructor(
    private readonly connection: Connection,
    private readonly userRepository: UserRepository,
    private readonly collaborationRepository: CollaborationRepository,
    private readonly collaborationMemberRepository: CollaborationMemberRepository,
  ) {
    connection.subscribers.push(this);
  }

  /**
   * Indicates that this subscriber only listen to Post events.
   */
  listenTo() {
    return User;
  }

  /**
   * Called before post insertion.
   */
  afterInsert(event: InsertEvent<User>) {
    setImmediate(async () => {
      this.logger.log(`After USER INSERTED: ${event.entity}`);
      const defaultCollab = new Collaboration();
      defaultCollab.name = 'default';
      await this.collaborationRepository.save(defaultCollab);
      const member = new CollaborationMember();
      member.collaboration = Promise.resolve(defaultCollab);
      member.user = Promise.resolve(event.entity);
      member.role = CollabRolesEnum.admin;
      await this.collaborationMemberRepository.save(member);
      event.entity.defaultCollaboration = Promise.resolve(defaultCollab);
      this.userRepository.save(event.entity);
    });
  }
}
