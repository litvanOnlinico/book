import { Command, Positional } from 'nestjs-command';
import { Inject, Injectable, Logger } from '@nestjs/common';
import {
  loadEntityFactories,
  loadSeeds,
  runSeed,
  setConnection,
} from 'typeorm-seeding';
import { Connection } from 'typeorm';
import chalk from 'chalk';
import { UserSeed } from './seeds/user.seed';

@Injectable()
export class SeedCommand {
  private readonly logger = new Logger(SeedCommand.name);
  constructor(
    private readonly connection: Connection,
  ) {}

  private readonly factoryPath = 'dist/database/factories/';

  // Get cli parameter for a different seeds path
  private readonly seedsPath = 'dist/database/seeds/';

  @Command({ command: 'seed:all', describe: 'create a user' })
  async create() {

    let factoryFiles;
    let seedFiles;
    try {
      factoryFiles = await loadEntityFactories(this.factoryPath);
      seedFiles = await loadSeeds(this.seedsPath);
    } catch (error) {
      return this.logger.log(error);
    }

    setConnection(this.connection);
    const seed = UserSeed;
    await runSeed(seed);

    // // Show seeds in the console
    // for (const seedFile of seedFiles) {
    //   try {
    //     let className = seedFile.split('/')[seedFile.split('/').length - 1];
    //     className = className.replace('.ts', '').replace('.js', '');
    //     className = className.split('-')[className.split('-').length - 1];
    //     this.logger.log(
    //       `${chalk.gray('executing seed:')} ${chalk.green.bold(
    //         `${className}`,
    //       )}`,
    //     );
    //     const seedFileObject: any = require(seedFile);
    //     await runSeed(seedFileObject);
    //   } catch (error) {
    //     this.logger.error('Could not run seed ', error);
    //   }
    // }

    this.logger.log(`👍 ${chalk.gray.underline(`finished seeding`)}`);
  }
}
