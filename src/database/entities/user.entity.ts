import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BeforeInsert,
  BeforeUpdate,
  OneToMany,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { User as IUser } from '../../graphql.schema';
import bcrypt from 'bcrypt';
import { Collaboration } from './collaboration.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 255 })
  name: string;
  @Column({
    unique: true,
    length: 255,
    transformer: {
      to(value: string): string {
        return value.toLowerCase();
      },
      from(value: string): string {
        return value;
      },
    },
  })
  email: string;
  @Column({ length: 255 })
  firstName: string;
  @Column({ length: 255 })
  lastName: string;
  @Column({
    length: 255,
  })
  password: string;
  @Column({ default: false })
  emailIsConfirmed: boolean;

  @OneToOne(type => Collaboration, {
    lazy: true,
  })
  @JoinColumn()
  defaultCollaboration: Promise<Collaboration>;

  // @OneToMany(type => Collaboration, collaboration => collaboration., {
  //   lazy: true,
  // })
  // public collaborations: Promise<Collaboration[]>;

  public isValidPassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }

  @BeforeInsert()
  @BeforeUpdate()
  public async managePassword() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
  }
}
