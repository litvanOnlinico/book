import { User } from './user.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CollaborationMember } from './collaboration-member.entity';

@Entity()
export class Collaboration {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ length: 255 })
  name: string;
  @OneToMany(type => CollaborationMember, members => members.collaboration)
  members: Promise<CollaborationMember>;

  currentMember: CollaborationMember;
}
