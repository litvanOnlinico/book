import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';
import { Collaboration } from './collaboration.entity';

export enum CollabRolesEnum {
  admin = 'ROLE_ADMIN',
  manager = 'ROLE_MANAGER',
}

@Entity()
export class CollaborationMember {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ type: 'enum', enum: CollabRolesEnum })
  role: CollabRolesEnum;
  @ManyToOne(type => User)
  user: Promise<User>;
  @ManyToOne(type => Collaboration, { lazy: true })
  collaboration: Promise<Collaboration>;
}
