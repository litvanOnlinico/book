import { EntityRepository, Repository } from 'typeorm';
import { CollaborationMember } from '../entities/collaboration-member.entity';

@EntityRepository(CollaborationMember)
export class CollaborationMemberRepository extends Repository<CollaborationMember> {}
