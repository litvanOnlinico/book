import { EntityRepository, Repository } from 'typeorm';
import { User } from '../entities/user.entity';
import { Collaboration } from '../entities/collaboration.entity';

@EntityRepository(Collaboration)
export class CollaborationRepository extends Repository<Collaboration> {}
