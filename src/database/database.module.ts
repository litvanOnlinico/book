import { Module } from '@nestjs/common';
import { databaseProviders } from './database.providers';
import { ConfigModule } from '../config/config.module';
import { SeedCommand } from './seed.command';
import { UserSubscriber } from './subscribers/user.subscriber';

@Module({
  imports: [ConfigModule],
  providers: [...databaseProviders, SeedCommand, UserSubscriber],
  exports: [...databaseProviders, SeedCommand],
})
export class DatabaseModule {}
