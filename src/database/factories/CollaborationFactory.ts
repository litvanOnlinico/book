import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import * as uuid from 'uuid';
import { Collaboration } from '../entities/collaboration.entity';

define(Collaboration, (faker: typeof Faker) => {
  const collaboration = new Collaboration();
  collaboration.id = uuid.v4();
  collaboration.name = faker.company.companyName();
  return collaboration;
});
