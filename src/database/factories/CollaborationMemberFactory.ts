import * as Faker from 'faker';
import { define } from 'typeorm-seeding';
import * as uuid from 'uuid';
import {
  CollaborationMember,
  CollabRolesEnum,
} from '../entities/collaboration-member.entity';

define(CollaborationMember, (
  faker: typeof Faker,
  settings: { role: CollabRolesEnum },
) => {
  const member = new CollaborationMember();
  member.id = uuid.v4();
  member.role = settings.role;
  return member;
});
