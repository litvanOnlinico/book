import { Connection, createConnection } from 'typeorm';
import { ConfigService } from '../config/config.service';
import { UserRepository } from './repositories/user.repository';
import { CollaborationRepository } from './repositories/collaboration.repository';
import { CollaborationMemberRepository } from './repositories/collaboration-member.repository';

export const databaseProviders = [
  {
    provide: Connection,
    useFactory: async (configService: ConfigService) =>
      await createConnection({
        dropSchema: configService.databaseDropSchema,
        type: 'postgres',
        host: configService.databaseHost,
        port: configService.databasePort,
        username: configService.databaseUsername,
        password: configService.databasePassword,
        database: configService.databaseName,
        entities: [__dirname + '/../**/*.entity{.ts,.js}'],
        synchronize: configService.databaseSynchronize,
        logging: false,
      }),
    inject: [ConfigService],
  },
  {
    provide: UserRepository,
    useFactory: (connection: Connection) => connection.getCustomRepository(UserRepository),
    inject: [Connection],
  },
  {
    provide: CollaborationRepository,
    useFactory: (connection: Connection) => connection.getCustomRepository(CollaborationRepository),
    inject: [Connection],
  },
  {
    provide: CollaborationMemberRepository,
    useFactory: (connection: Connection) => connection.getCustomRepository(CollaborationMemberRepository),
    inject: [Connection],
  },
];
