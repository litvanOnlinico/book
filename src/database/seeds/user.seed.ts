import { Factory, Seed, times } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';
import { User } from '../entities/user.entity';
import {
  CollaborationMember,
  CollabRolesEnum,
} from '../entities/collaboration-member.entity';
import { Collaboration } from '../entities/collaboration.entity';

export class UserSeed implements Seed {
  public async seed(factory: Factory, connection: Connection): Promise<any> {
    const em = connection.createEntityManager();

    await times(10, async n => {
      const user = await factory(User)().seed();
      const collaboration = await factory(Collaboration)().seed();

      const member = await factory(CollaborationMember)({
        role: CollabRolesEnum.admin,
      }).make();
      member.user = Promise.resolve(user);
      member.collaboration = Promise.resolve(collaboration);
      await em.save(member);
    });
  }
}
