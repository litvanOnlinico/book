import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { DatabaseModule } from '../database/database.module';
import { AuthModule } from '../auth/auth.module';
import { CollaborationsResolvers } from './collaborations.resolvers';
import { CollaborationsService } from './collaborations.service';

@Module({
  imports: [ConfigModule, DatabaseModule, forwardRef(() => AuthModule)],
  providers: [CollaborationsResolvers, CollaborationsService],
})
export class CollaborationsModule {}
