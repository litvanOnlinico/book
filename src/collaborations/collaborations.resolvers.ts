import { Injectable, UseGuards } from '@nestjs/common';
import { Resolver, Query } from '@nestjs/graphql';
import { CollaborationsService } from './collaborations.service';
import { Collaboration } from '../database/entities/collaboration.entity';
import { GqlAuthGuard } from '../auth/guard/gql-auth.guard';

@Injectable()
@Resolver('Collaboration')
export class CollaborationsResolvers {
  constructor(private readonly collaborationsService: CollaborationsService) {}

  @UseGuards(GqlAuthGuard)
  @Query()
  async collaborations(root: any, args: any, context: any) {
    return this.collaborationsService.findAllForUser(context.req.user);
  }
}
