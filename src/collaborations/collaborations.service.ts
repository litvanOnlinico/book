import { Inject, Injectable, Logger } from '@nestjs/common';
import { Connection, EntitySubscriberInterface, EventSubscriber, InsertEvent, Repository } from 'typeorm';
import { Collaboration } from '../database/entities/collaboration.entity';
import { CollaborationMember, CollabRolesEnum } from '../database/entities/collaboration-member.entity';
import { User } from '../database/entities/user.entity';
import { CollaborationRepository } from '../database/repositories/collaboration.repository';
import { CollaborationMemberRepository } from '../database/repositories/collaboration-member.repository';
import { UserRepository } from '../database/repositories/user.repository';

@Injectable()
@EventSubscriber()
export class CollaborationsService {
  private readonly logger = new Logger(CollaborationsService.name);

  constructor(private readonly collaborationRepository: CollaborationRepository) {}

  findAllForUser(user: User): Promise<Collaboration[]> {
    return this.collaborationRepository
      .createQueryBuilder('collab')
      .leftJoinAndMapOne('collab.currentMember', 'collab.members', 'member', `member.user = :userId`, {
        userId: user.id,
      })
      .leftJoin(CollaborationMember, 'members', 'members.collaboration = collab.id')
      .where('members.user = :userId', {
        userId: user.id,
      })
      .getMany();
  }
}
