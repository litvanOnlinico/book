import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
  @Get()
  findAll() {
    return 'This action returns all cats';
  }

  @Get('me')
  // @UseGuards(AuthGuard('bearer'))
  @UseGuards(AuthGuard())
  me() {
    return 'It\'s me';
  }
}
