import { IsEmail, IsNotEmpty } from 'class-validator';
import { InputCreateUser } from '../../graphql.schema';

export class CreateUserDto extends InputCreateUser {
  @IsEmail()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  firstName: string;
  @IsNotEmpty()
  lastName: string;
}
