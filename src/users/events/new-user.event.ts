import { IEvent } from '@nestjs/cqrs';
import { User } from '../../database/entities/user.entity';

export class NewUserEvent implements IEvent {
  constructor(public readonly user: User) {}
}
