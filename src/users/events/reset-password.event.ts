import { IEvent } from '@nestjs/cqrs';
import { User } from '../../database/entities/user.entity';

export class ResetPasswordEvent implements IEvent {
  constructor(public readonly user: User) {}
}
