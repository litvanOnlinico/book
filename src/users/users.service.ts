import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { EventBus } from '@nestjs/cqrs';
import { ResetPasswordEvent } from './events/reset-password.event';
import { NewUserEvent } from './events/new-user.event';
import { UserRepository } from '../database/repositories/user.repository';

@Injectable()
export class UsersService {
  constructor(private readonly userRepository: UserRepository, private readonly eventBus: EventBus) {}

  findOneByEmail(email: string): Promise<User> {
    return this.userRepository.findOne({ email: email.toLowerCase() });
  }

  findOneById(id: string) {
    return this.userRepository.findOne(id);
  }

  async createUser(user: CreateUserDto) {
    const newUser = await this.userRepository.create(user);
    await this.userRepository.save(newUser);
    this.eventBus.publish(new NewUserEvent(newUser));
    return newUser;
  }

  async resetPassword(email: string): Promise<boolean> {
    // const user = await this.userRepository.findOneOrFail({
    //   email: email.toLowerCase(),
    // });

    this.eventBus.publish(new ResetPasswordEvent(null));
    return true;
  }
}
