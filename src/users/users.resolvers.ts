import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { InputLoginUser, User } from '../graphql.schema';
import { CreateUserDto } from './dto/create-user.dto';
import { Injectable, UseGuards, UseInterceptors } from '@nestjs/common';
import { GqlAuthGuard } from '../auth/guard/gql-auth.guard';
import { UsersService } from './users.service';
import { AuthService } from '../auth/auth.service';
import { ModuleRef } from '@nestjs/core';
import { LoggingInterceptor } from './logging.interceptor';
import { Ability, AbilityBuilder, Rule } from '@casl/ability';
import { rulesToQuery } from '@casl/ability/extra';

@Injectable()
@Resolver('User')
export class UsersResolvers {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Query('me')
  @UseGuards(GqlAuthGuard)
  async me(root: any, args: any, context: any) {
    return context.req.user;
  }

  @Query()
  async login(@Args('input') login: InputLoginUser) {
    // const adminAbility = AbilityBuilder.define((can: any, cannot: any) => {
    //   can('read', 'Post');
    // });
    //
    // function ruleToQuery(rule: Rule) {
    //   if (JSON.stringify(rule.conditions).includes('"$all":')) {
    //     throw new Error('Sequelize does not support "$all" operator');
    //   }
    //
    //   return rule.inverted ? { $not: rule.conditions } : rule.conditions;
    // }
    //
    // const res = adminAbility.rules;
    //
    // const desirialized = new Ability(res);

    return this.authService.signIn(login);
  }

  // @UseInterceptors(LoggingInterceptor)
  @Query()
  async resetPassword(@Args('email') email: string) {
    return this.usersService.resetPassword(email);
  }

  @Mutation('createUser')
  async create(@Args('input') user: CreateUserDto): Promise<User> {
    return this.usersService.createUser(user);
  }
}
