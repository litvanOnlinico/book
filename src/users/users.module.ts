import { forwardRef, Module, OnModuleInit } from '@nestjs/common';
import { UsersResolvers } from './users.resolvers';
import { UsersController } from './users.controller';
import { AuthModule } from '../auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { DatabaseModule } from '../database/database.module';
import { UsersService } from './users.service';
import { ConfigModule } from '../config/config.module';
import { CQRSModule, EventBus } from '@nestjs/cqrs';
import { CollaborationsModule } from '../collaborations/collaborations.module';

@Module({
  imports: [
    CQRSModule,
    CollaborationsModule,
    ConfigModule,
    DatabaseModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    forwardRef(() => AuthModule),
  ],
  controllers: [UsersController],
  providers: [UsersResolvers, UsersService],
  exports: [UsersService],
})
export class UsersModule {}
