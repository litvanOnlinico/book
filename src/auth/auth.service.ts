import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { LoginUserDto } from '../users/dto/login-user.dto';
import { User } from '../database/entities/user.entity';
import { AbilityBuilder } from '@casl/ability';

@Injectable()
export class AuthService {
  // readonly adminAbility = AbilityBuilder.define((can: any, cannot: any) => {
  //   can('read', 'all');
  // });
  //
  // function defineAbilitiesFor(user) {
  //   switch (user.role) {
  //     case 'admin':
  //       return adminAbility
  //     default:
  //       return null
  //   }
  // }

  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(login: LoginUserDto): Promise<string> {
    const user = await this.usersService.findOneByEmail(login.email);
    if (!user || (await user.isValidPassword(login.password)) === false) {
      throw new UnauthorizedException();
    }
    const payload: JwtPayload = { id: user.id };
    return this.jwtService.sign(payload);
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.usersService.findOneById(payload.id);
  }
}
