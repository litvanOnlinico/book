import { NestFactory } from '@nestjs/core';
import { CommandModule, CommandService } from 'nestjs-command';
import { ApplicationModule } from './app.module';
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module'; // Base module

@Module({
  imports: [CommandModule, DatabaseModule],
})
class CliModule {}

(async () => {
  const app = await NestFactory.createApplicationContext(CliModule);
  app
    .select(CommandModule)
    .get(CommandService)
    .exec();
})();
