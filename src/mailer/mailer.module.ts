/* tslint:disable:max-classes-per-file */
import { DynamicModule, Module, OnModuleInit } from '@nestjs/common';
import { ResetPasswordHandler } from './handlers/reset-password.handler';
import { ModuleRef } from '@nestjs/core';
import { MailerService } from './mailer.service';
import { CQRSModule, EventBus } from '@nestjs/cqrs';
import { AuthModule } from '../auth/auth.module';
import { MailerModuleOptions } from './mailer-module-options.service';
import { SendGridProvider } from './providers/send-grid-provider.service';
import { NewUserHandler } from './handlers/new-user.handler';

const handlers = [ResetPasswordHandler, NewUserHandler];

export enum MailerServiceEnum {
  sendGrid = 'SendGridToken',
}

export interface IMailerModuleOptions {
  mailService: MailerServiceEnum;
}

@Module({})
export class MailerModule implements OnModuleInit {
  static register(options: IMailerModuleOptions): DynamicModule {
    let mailerClass = null;

    switch (options.mailService) {
      case MailerServiceEnum.sendGrid:
        mailerClass = SendGridProvider;
        break;
      default:
        mailerClass = SendGridProvider;
    }

    return {
      module: MailerModule,
      imports: [CQRSModule, AuthModule],
      providers: [
        ...handlers,
        { provide: MailerModuleOptions, useValue: options },
        { provide: 'MailProviderToken', useClass: mailerClass },
        MailerService,
      ],
    };
  }

  constructor(
    private readonly event$: EventBus,
    private readonly moduleRef: ModuleRef,
  ) {}

  onModuleInit(): any {
    this.event$.setModuleRef(this.moduleRef);
    this.event$.register(handlers);
  }
}
