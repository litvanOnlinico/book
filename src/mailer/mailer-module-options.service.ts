import { Injectable } from '@nestjs/common';
import { IMailerModuleOptions, MailerServiceEnum } from './mailer.module';

@Injectable()
export class MailerModuleOptions implements IMailerModuleOptions {
  mailService: MailerServiceEnum;
}
