import { Injectable, Logger } from '@nestjs/common';
import { IMailerProvider } from '../mailer.service';
import sgMail from '@sendgrid/mail';
import { ConfigService } from '../../config/config.service';
import IMailerMessage from '../interfaces/mailer-message.interface';

@Injectable()
export class SendGridProvider implements IMailerProvider {
  private readonly logger = new Logger(SendGridProvider.name);
  constructor(private readonly configService: ConfigService) {
    sgMail.setApiKey(configService.sendGridToken);
  }

  async send(msg: IMailerMessage): Promise<void> {
    this.logger.log(`send ${JSON.stringify(msg)}`);
    // await sgMail.send(msg);
  }
}
