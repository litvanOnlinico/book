import { Injectable } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { NewUserEvent } from '../../users/events/new-user.event';
import { MailerService } from '../mailer.service';

@Injectable()
@EventsHandler(NewUserEvent)
export class NewUserHandler implements IEventHandler<NewUserEvent> {
  constructor(private readonly mailerService: MailerService) {}

  async handle(event: NewUserEvent) {
    await this.mailerService.sendConfirmationEmail(event.user);
  }
}
