import { Injectable } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ResetPasswordEvent } from '../../users/events/reset-password.event';
import { MailerService } from '../mailer.service';

@Injectable()
@EventsHandler(ResetPasswordEvent)
export class ResetPasswordHandler implements IEventHandler<ResetPasswordEvent> {
  constructor(private readonly mailerService: MailerService) {}

  async handle(event: ResetPasswordEvent): Promise<any> {
    // tslint:disable-next-line:no-console
    await this.mailerService.sendResetPassword(event.user);
  }
}
