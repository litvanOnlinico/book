import { Interface } from 'readline';
import { string } from 'joi';

export default interface IMailerMessage {
  to: string;
  from: string;
  subject: string;
  html: string;
}
