import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { User } from '../database/entities/user.entity';
import IMailerMessage from './interfaces/mailer-message.interface';

export interface IMailerProvider {
  send(message: IMailerMessage): Promise<void>;
}

@Injectable()
export class MailerService {
  private readonly logger = new Logger(MailerService.name);
  constructor(
    @Inject('MailProviderToken')
    private readonly mailerProvider: IMailerProvider,
  ) {}

  async sendResetPassword(user: User): Promise<void> {
    this.logger.log('sendResetPassword');

    const msg = {
      to: 'litvan88@gmail.com',
      from: 'test@example.com',
      subject: 'Sending with SendGrid is Fun',
      html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    };

    await this.mailerProvider.send(msg);
  }

  async sendConfirmationEmail(user: User) {
    this.logger.log('sendConfirmationEmail');

    const msg = {
      to: 'litvan88@gmail.com',
      from: 'test@example.com',
      subject: 'Sending with SendGrid is Fun',
      html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    };

    await this.mailerProvider.send(msg);
  }
}
