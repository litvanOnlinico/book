import { LoggerService } from '@nestjs/common';
import pino from 'pino';

export class PinoLogger implements LoggerService {
  private readonly logger = pino({
    prettyPrint: process.env.NODE_ENV !== 'production',
  });

  error(message: any, trace?: string, context?: string): any {
    this.logger.error(`[${context}] ${message}`, trace);
  }

  log(message: any, context?: string): any {
    this.logger.info(`[${context}] ${message}`);
  }

  warn(message: any, context?: string): any {
    this.logger.warn(`[${context}] ${message}`);
  }
}
